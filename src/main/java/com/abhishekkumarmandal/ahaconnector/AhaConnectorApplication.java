package com.abhishekkumarmandal.ahaconnector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AhaConnectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(AhaConnectorApplication.class, args);
	}

}
