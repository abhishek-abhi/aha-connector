package com.abhishekkumarmandal.ahaconnector.service;

import java.util.List;

import com.abhishekkumarmandal.ahaconnector.service.modal.ConnectEntity;

public class ConnectorServiceImpl {

	ConnectorService connectorService;
	
	public Object saveFeature(ConnectEntity object) {
		return connectorService.save(object);
	}
	
	public List<ConnectEntity> fetchFeatures(){
		return connectorService.findAll();
	}
	
}
