package com.abhishekkumarmandal.ahaconnector.service.modal;

public class DataModel {
    private String id;
    private String name;
	public DataModel() {
		super();
	}
	public DataModel(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return String.format("DataModel [id=%s, name=%s]", id, name);
	}
    
}
