package com.abhishekkumarmandal.ahaconnector.service.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.vladmihalcea.hibernate.type.json.JsonStringType;

@Entity
@TypeDefs({
	  @TypeDef(name = "json", typeClass = JsonStringType.class)
	})
public class ConnectEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Type(type = "json")
	@Column(columnDefinition = "json")
	private String content;
	
	public ConnectEntity() {
		
	}
	
	public ConnectEntity(String content) {
		this.content = content;
	}
	
	public ConnectEntity(long id,String content) {
		this.id = id;
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getReference() {
		return content;
	}

	public void setReference(String reference) {
		this.content = reference;
	}

	@Override
	public String toString() {
		return String.format("ConnectEntity [id=%s, content=%s]", id, content);
	}
	
}
