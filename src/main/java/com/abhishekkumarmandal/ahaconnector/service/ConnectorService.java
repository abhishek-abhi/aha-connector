package com.abhishekkumarmandal.ahaconnector.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.abhishekkumarmandal.ahaconnector.service.modal.ConnectEntity;
import com.abhishekkumarmandal.ahaconnector.service.modal.Feature;

@Repository
public interface ConnectorService extends JpaRepository<ConnectEntity, Long> {

}
