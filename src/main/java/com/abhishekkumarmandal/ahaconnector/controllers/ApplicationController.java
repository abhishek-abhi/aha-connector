package com.abhishekkumarmandal.ahaconnector.controllers;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

@RestController
public class ApplicationController {

	List<Object> contents = new ArrayList();
	HashMap<String, Object> contentDetails = new HashMap();
	String response;
	String responseContents;

	@PostMapping("/webhook")
	public ResponseEntity<String> fetchUpdates(@RequestBody String obj) {
		Gson json = new Gson();
		Object element = json.fromJson(obj, Object.class);
		contents.add(element);
		return new ResponseEntity<String>(obj, HttpStatus.CREATED);
	}

	@GetMapping("/contents")
	public ResponseEntity<List<Object>> fetchAllFeatures() {
		return new ResponseEntity<List<Object>>(contents, HttpStatus.OK);
	}

	@GetMapping("/")
	public String greetingsFeature() {
		return "Feature Integration";
	}

	@GetMapping("/projects")
	public ResponseEntity<Object> fetchAllProjects() {
		String credentials = "abhishekkumarmandal06@gmail.com:KZ53EfOpNkHLcwbI9Owr43B6";
		byte[] encodedCredential = Base64.getEncoder().encode(credentials.getBytes());
		String authorizationToken = new String(encodedCredential);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + authorizationToken);
		HttpEntity request = new HttpEntity(headers);
		return new RestTemplate().exchange("https://abhishek-kumar-mandal.atlassian.net/rest/api/2/project",
				HttpMethod.GET, request, Object.class);
	}

	/*
	 * @GetMapping("/initiatives") public String fetchInitiatives() { HttpHeaders
	 * headers = new HttpHeaders(); // String authorizationToken = //
	 * "7a233e5c1a5e7c7de8647c9d7dda0ce15e75a7b6212904b86c645645a561ad03"; String
	 * authorizationToken =
	 * "d46f1677e788861a64c7184be272bf6a0e267d327a26167af5656ff121a097d5";
	 * headers.add("Authorization", "Bearer " + authorizationToken); HttpEntity
	 * request = new HttpEntity(headers); // ResponseEntity<String> objectResponse =
	 * new // RestTemplate().exchange(
	 * "https://abhishek-kumar-mandal.aha.io/api/v1/initiatives",HttpMethod.GET, //
	 * request, String.class); ResponseEntity<String> objectResponse = new
	 * RestTemplate().exchange(
	 * "https://sakshi270396.aha.io/api/v1/initiatives/IT-S-2", HttpMethod.GET,
	 * request, String.class); response = objectResponse.getBody();
	 * processInitiative(response); // processInitiatives(response); return
	 * response; }
	 * 
	 * @GetMapping("/initiatives/{query}") public ResponseEntity<Object>
	 * displayContent(@PathVariable String query) { String value =
	 * contentDetails.get(query).toString(); return new
	 * ResponseEntity<Object>(value, HttpStatus.OK); }
	 * 
	 * public Object processResponse(String value, String query) { JSONObject data =
	 * new JSONObject(value); Iterator<String> keys = data.keys(); while
	 * (keys.hasNext()) { String key = keys.next(); String result =
	 * data.get(key).toString(); if (key.contains(query)) {
	 * System.out.println("**************"); responseContents = result; }
	 * System.out.println("key : " + key); System.out.println(result); } return
	 * responseContents; }
	 * 
	 * @GetMapping("/initiatives/{query1}/{query2}") public ResponseEntity<Object>
	 * displayContent(@PathVariable String query1, @PathVariable String query2) {
	 * 
	 * String value = contentDetails.get(query1).toString(); String regex = "[0-9]";
	 * Pattern p = Pattern.compile(regex); Matcher m = p.matcher(query2);
	 * if(m.matches()) { JSONArray responses = (JSONArray)
	 * contentDetails.get(query1); int index = Integer.parseInt(query2); return new
	 * ResponseEntity<Object>(responses.get(index).toString(),HttpStatus.OK); } else
	 * { Object data = processResponse(value,query2); return new
	 * ResponseEntity<Object>(data, HttpStatus.OK); } }
	 * 
	 * @GetMapping("/initiatives/{query1}/{query2}/{query3}") public
	 * ResponseEntity<Object> displayContents(@PathVariable String
	 * query1, @PathVariable String query2, @PathVariable String query3) { String
	 * value = displayContent(query1, query2).getBody().toString(); String regex =
	 * "[0-9]+"; Pattern p = Pattern.compile(regex); Matcher m = p.matcher(query3);
	 * if(m.matches()) { JSONArray responses = (JSONArray)
	 * contentDetails.get(query3); int index = Integer.parseInt(query3); return new
	 * ResponseEntity<Object>(responses.get(index).toString(),HttpStatus.OK); } else
	 * { Object data = processResponse(value,query3); return new
	 * ResponseEntity<Object>(data, HttpStatus.OK); } }
	 * 
	 * @GetMapping("/initiatives/{query1}/{query2}/{query3}/{query4}") public
	 * ResponseEntity<Object> displayContents(@PathVariable String
	 * query1, @PathVariable String query2, @PathVariable String
	 * query3, @PathVariable String query4) { String value = displayContents(query1,
	 * query2, query3).getBody().toString(); String regex = "[0-9]+"; Pattern p =
	 * Pattern.compile(regex); Matcher m = p.matcher(query4); if(m.matches()) {
	 * JSONArray responses = (JSONArray) contentDetails.get(query3); int index =
	 * Integer.parseInt(query4); return new
	 * ResponseEntity<Object>(responses.get(index).toString(),HttpStatus.OK); } else
	 * { Object data = processResponse(value,query4); return new
	 * ResponseEntity<Object>(data, HttpStatus.OK); } }
	 * 
	 * @GetMapping("/initiatives/{query1}/{query2}/{query3}/{query4}/{query5")
	 * public ResponseEntity<Object> displayContents(@PathVariable String
	 * query1, @PathVariable String query2, @PathVariable String
	 * query3, @PathVariable String query4, @PathVariable String query5) { String
	 * value = displayContents(query1, query2, query3, query4).getBody().toString();
	 * String regex = "[0-9]+"; Pattern p = Pattern.compile(regex); Matcher m =
	 * p.matcher(query5); if(m.matches()) { JSONArray responses = (JSONArray)
	 * contentDetails.get(query3); int index = Integer.parseInt(query5); return new
	 * ResponseEntity<Object>(responses.get(index).toString(),HttpStatus.OK); } else
	 * { Object data = processResponse(value,query5); return new
	 * ResponseEntity<Object>(data, HttpStatus.OK); } }
	 * 
	 * public void processInitiative(String object) { JSONObject jsonObject = new
	 * JSONObject(object); JSONObject data = jsonObject.getJSONObject("initiative");
	 * Iterator<String> keys = data.keys(); while (keys.hasNext()) { String key =
	 * keys.next(); Object value = data.get(key); System.out.println(key);
	 * System.out.println(value); contentDetails.put(key, value);
	 * System.out.println("======================"); } }
	 * 
	 * public void processInitiatives(String object) { JSONObject jsonObject = new
	 * JSONObject(object); JSONArray jsonArray =
	 * jsonObject.getJSONArray("initiatives"); for (int i = 0; i <
	 * jsonArray.length(); i++) { JSONObject data = jsonArray.getJSONObject(i);
	 * System.out.println(data); System.out.println("====================");
	 * Iterator<String> keys = data.keys(); while (keys.hasNext()) { String key =
	 * keys.next(); String value = data.get(key).toString();
	 * System.out.println(key); System.out.println(value);
	 * 
	 * Gson json = new Gson(); Object element = json.fromJson(value, Object.class);
	 * 
	 * contentDetails.put(key, value); System.out.println("======================");
	 * } } }
	 */

}
